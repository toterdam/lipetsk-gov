import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default ()=> new Vuex.Store({
  state:{
    news: [
      {
        id: 1,
        dateofpublication: "12 авг 2020",
        newsTitle: "Круто мыслить позитивно, быть здоровым, жить активно!",
        dscription: "Сегодня в свой обеденный перерыв сотрудники управления государственной службы и кадровой работы администрации Липецкой области...",
        newsimage: "image25.png"
      },
      {
        id:2,
        dateofpublication: "12 авг 2020",
        newsTitle: "Предоставление муниципальных услуг в центрах “Мои документы” обретает единый формат",
        dscription: "",
        newsimage: "image26.png"
      },
      {
        id:3,
        dateofpublication: "12 авг 2020",
        newsTitle: "Состоялось заседание hr-клуба по вопросам повышения эффективности качества работы hr-служб в подведомственных учреждениях области",
        dscription: "",
        newsimage: "image26.png"
      },
      {
        id:4,
        dateofpublication: "12 авг 2020",
        newsTitle: "Липецкая область участвует в ярмарке вакансий в онлайн-формате",
        dscription: "",
        newsimage: "image26.png"
      },
      {
        id:5,
        dateofpublication: "12 авг 2020",
        newsTitle: "В рамках hr-форума стартует новый проект",
        dscription: "",
        newsimage: "image26.png"
      },
      {
        id:6,
        dateofpublication: "12 авг 2020",
        newsTitle: "В рамках hr-форума стартует новый проект",
        dscription: "",
        newsimage: "image26.png"
      },
      {
        id:7,
        dateofpublication: "12 авг 2020",
        newsTitle: "В рамках hr-форума стартует новый проект",
        dscription: "",
        newsimage: "image26.png"
      },
    ],
    structures:[
      {
        name: "Отдел кадровой работы",
        structureicon: "document.png",
      },
      {
        name: "Отдел государственной службы",
        structureicon: "book(6).png",
      },
      {
        name: "Отдел кадровой экспертизы",
        structureicon: "folder.png",
      },
      {
        name: "Отдел наградной работы",
        structureicon: "prize1.png",
      },
      {
        name: "Отдел развития кадрового потенциала",
        structureicon: "growth.png",
      },
    ]

  },
  getters: {
    getNews: state => state.news,
    getstructures: state => state.structures
  }
})
